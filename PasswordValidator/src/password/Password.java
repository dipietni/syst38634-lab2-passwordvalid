/* Nicholas Di Pietrantonio - 991444656*/
package password;

public class Password {

	public static void main(String[] args) {

	}
	public static boolean validateUpperCase(String password) throws Exception {
		boolean containsUpper = false;
		boolean containsLower = false;
		for (int i = 0 ; i < password.length(); i++) {
			if (Character.isUpperCase(password.charAt(i))) {
				containsUpper = true;
			} else {
				containsLower = true;
			}
		}
		if (containsUpper == false || containsLower == false) {
			throw new Exception("password needs upper and lower case letters");
		}
		return true;
	}
	
	public static boolean validatePasswordLength(String password) throws Exception {
		if (password.length() < 8) {
			throw new Exception("password not long enough");
		}
		return true;
	}
	public static boolean validatePasswordNumbers(String password) throws Exception {
		int totalNumbers = 0;
		for (int i = 0 ; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i))) {
				totalNumbers++;
			}
		}
		if (totalNumbers < 2) {
			throw new Exception("not enough numbers");
		}
		return true;
	}

}
